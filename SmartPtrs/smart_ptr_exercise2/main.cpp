#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <memory>
#include <vector>
#include <string>
#include <boost/scoped_ptr.hpp>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}

};

class Broker 
{
public:
    Broker(int devno1, int devno2)
        : dev1_(new Device(devno1)),
          dev2_(new Device(devno2))
	{
	}

    Broker(const Broker& source)
        : dev1_(new Device(*source.dev1_)),
          dev2_(new Device(*source.dev2_))
    {}

private:
    boost::scoped_ptr<Device> dev1_;
    boost::scoped_ptr<Device> dev2_;
};

std::unique_ptr<Device> factory(int id)
{
    return std::unique_ptr<Device>(new Device(id));
}

int main()
{
    {
        boost::scoped_ptr<Device> ptr_dev(new Device(100));

        //may_throw();
    }

    std::vector<std::string> vec_str;

    vec_str.push_back(std::string("Text"));

    {
        std::unique_ptr<Device> ptr_dev1 = factory(13);
        std::unique_ptr<Device> copy_of_ptr1 = std::move(ptr_dev1);

        std::vector<std::unique_ptr<Device>> vec_dev;

        vec_dev.push_back(std::move(copy_of_ptr1));
    }


	try
	{
        Broker b(1, 3);

        Broker copy_of_b = b;
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
