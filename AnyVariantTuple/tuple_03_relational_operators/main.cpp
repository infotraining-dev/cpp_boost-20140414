#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/tuple/tuple_io.hpp>
#include "PersonalInfo.hpp"

using namespace std;

struct Data
{
    int x, y;
    string str;

    Data(int x, int y, const string& str) : x(x), y(y), str(str)
    {}

    bool operator==(const Data& other)
    {
        return boost::tie(x, y, str) == boost::tie(other.x, other.y, other.str);
    }

    bool operator<(const Data& other)
    {
        return boost::tie(x, y, str) < boost::tie(other.x, other.y, other.str);
    }
};

int main()
{
    typedef boost::tuple<string, int, PersonalInfo> MyTuple;

    cout << "Length of MyTuple: "
         << boost::tuples::length<MyTuple>::value << endl;

	boost::tuple<string, int, PersonalInfo> t1("same?", 2, PersonalInfo("Nikodem", "Dyzma", 28));
	boost::tuple<string, int, PersonalInfo> t2("same?", 2, PersonalInfo("Nikodem", "Dyzma", 28));
	boost::tuple<string, int, PersonalInfo> t3("different", 2, PersonalInfo("Nikodem", "Dyzma", 28));

    cout << boost::tuples::set_open('[') << boost::tuples::set_close(']')
        << boost::tuples::set_delimiter(',') << t1 << endl;

	cout.setf(ios::boolalpha);
	cout << t1 << " == " << t2 << ": "  << (t1 == t2) << endl;
	cout << t1 << " == " << t3 << ": "  << (t1 == t3) << endl;

	typedef boost::tuple<string, int, PersonalInfo> tuple_3;
	vector<tuple_3> vec_of_tuples;

	vec_of_tuples.push_back(boost::make_tuple("1_one", 1, PersonalInfo("Nikodem", "Dyzma", 28)));
	vec_of_tuples.push_back(boost::make_tuple("5_five", 1, PersonalInfo("Nikodem", "Anonim", 22)));
	vec_of_tuples.push_back(boost::make_tuple("1_one", 1, PersonalInfo("Nikodem", "Anonim", 28)));
	vec_of_tuples.push_back(boost::make_tuple("1_one", 2, PersonalInfo("Nikodem", "Dyzma", 28)));
	vec_of_tuples.push_back(boost::make_tuple("4_four", 1, PersonalInfo("Nikodem", "Dyzma", 28)));

	sort(vec_of_tuples.begin(), vec_of_tuples.end());

	cout << "Posortowane krotki:\n";
	copy(vec_of_tuples.begin(),
			vec_of_tuples.end(),
			ostream_iterator<tuple_3>(cout, "\n"));
	cout << "\n" << endl;

    Data d1(1, 2, "three");
    Data d2(1, 2, "three");

    cout << "d1 == d2: " << (d1 == d2) << endl;
    cout << "d1 < d2: " << (d1 < d2) << endl;
}
