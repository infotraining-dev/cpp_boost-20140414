#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/algorithm/cxx11/copy_if.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

auto ci_compare(const string& s1, const string& s2) -> bool
{
    return boost::to_lower_copy(s1) < boost::to_lower_copy(s2);
}

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    boost::algorithm::copy_if(employees.begin(), employees.end(),
                              ostream_iterator<Person>(cout, "\n"),
                              boost::bind(&Person::salary, _1) > 3000.0);
    double threshold = 3000.0;
    using namespace boost::algorithm;

    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            [&](const Person& p) { return p.salary() > threshold; });

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    boost::algorithm::copy_if(employees.begin(), employees.end(),
                              ostream_iterator<Person>(cout, "\n"),
                              boost::bind(&Person::age, _1) < 30);

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";

    sort(employees.begin(), employees.end(),
         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    sort(employees.begin(), employees.end(),
         [](const Person& p1, const Person& p2) -> bool
           { return ci_compare(p1.name(), p2.name()); });

//    sort(employees.begin(), employees.end(),
//         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    sort(employees.begin(), employees.end(),
         std::bind(&ci_compare,
                     std::bind(&Person::name, placeholders::_1),
                     std::bind(&Person::name, placeholders::_2)));

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    boost::algorithm::copy_if(employees.begin(), employees.end(),
                              ostream_iterator<Person>(cout, "\n"),
                              boost::bind(&Person::gender, _1) == Female);

    // ilość osob zarabiajacych powyżej średniej


    double avg
        = std::accumulate(employees.begin(), employees.end(), 0.0,
                          bind(plus<double>(),
                               _1,
                               boost::bind(&Person::salary, _2)))
                                    /  employees.size();

    cout << "\nAvg salary: " << avg << endl;
    cout << "Ilosc osob zarabiajacych powyzej sredniej:\n";
    cout << count_if(employees.begin(), employees.end(),
                     boost::bind(&Person::salary, _1) > avg) << endl;
}
