#include <iostream>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

template <typename Tuple, typename Func, int Index>
struct ForEachHelper
{
    static void call(const Tuple& t, Func f)
    {
        ForEachHelper<Tuple, Func, Index-1>::call(t, f);
        f(t.get<Index>());
    }
};

template <typename Tuple, typename Func>
struct ForEachHelper<Tuple, Func, 0>
{
    static void call(const Tuple& t, Func f)
    {
        f(t.get<0>());
    }
};

template <typename Tuple, typename Func>
void for_each_item(const Tuple& t, Func f)
{
    using namespace boost::tuples;
    ForEachHelper<Tuple, Func, length<Tuple>::value-1>::call(t, f);
}

class Printer
{
public:
    void operator()(int i) const
    {
        cout << "int: " << i << endl;
    }

    void operator()(double d) const
    {
        cout << "double: " << d << endl;
    }

    void operator()(const string& i) const
    {
        cout << "string: " << i << endl;
    }
};

struct Data
{
    int x;
    double y;
    string str;
};

int main()
{
    tuple<int, double, string> t1(1, 5, "text");

    for_each_item(t1, Printer());

//    cout << "int:" << t1.get<0>() << endl;
//    cout << "double:" << t1.get<1>() << endl;
//    cout << "string:" << t1.get<2>() << endl;
}

