#ifndef FRACTION_HPP
#define FRACTION_HPP

// dołącza standardowe pliki nagłówkowe
#include <iostream>

class Fraction {

  private:
    int numer;
    int denom;

  public:
    /* nowość: klasa wyjątku
     */
    class DenomIsZero {
    };

    /* domyślny konstruktor i konstruktor o jednym i dwóch parametrach
     */
    Fraction(int = 0, int = 1);

    /* mnożenie
     * - nowość: globalna funkcja zaprzyjaźniona, umożliwia
     * automatyczną konwersję typu dla pierwszego argumentu
     */
    friend Fraction operator * (const Fraction&, const Fraction&);

    // przypisanie z mnożeniem
    const Fraction& operator *= (const Fraction&);

    /* porównanie
     * - nowość: globalna funkcja zaprzyjaźniona, umożliwia
     * automatyczną konwersję typu dla pierwszego argumentu
     */
    friend bool operator < (const Fraction&, const Fraction&);

    // zapis i odczyt ze strumienia
    void printOn(std::ostream&) const;
    void scanFrom(std::istream&);

    // jawna konwersja typu do double
    double toDouble() const;
};

/* operator *
 * - globalna funkcja zaprzyjaźniona
 * - zdefiniowany jako funkcja rozwijana w miejscu wywołania
 */
inline Fraction operator * (const Fraction& a, const Fraction& b)
{
    /* mnoży liczniki i mianowniki
     * - nadal nie skraca ułamka
     */
    return Fraction(a.numer * b.numer, a.denom * b.denom);
}

/* standardowy operator wyjścia
 * - globalnie przeciążony i zdefiniowany jako funkcja rozwijana w miejscu
 */
inline
std::ostream& operator << (std::ostream& strm, const Fraction& f)
{
    f.printOn(strm);    // wywołuje funkcję składową
    return strm;        // zwraca strumień umożliwiając tworzenie łańcuchów wywołań
}

/* standardowy operator wejścia
 * - globalnie przeciążony i zdefiniowany jako funkcja rozwijana w miejscu
 */
inline
std::istream& operator >> (std::istream& strm, Fraction& f)
{
    f.scanFrom(strm);   // wywołuje funkcję składową
    return strm;        // zwraca strumień umożliwiając tworzenie łańcuchów wywołań
}

#endif  // FRACTION_HPP

