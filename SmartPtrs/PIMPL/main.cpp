#include <iostream>
#include <memory>
#include "my_class.hpp"
#include <boost/scoped_array.hpp>

using namespace std;

//legacy api

char* alloc_buffer(size_t size)
{
    return new char[size];
}


int main()
{
    auto_ptr<IMyClass> ptr_myclass(new MyClass(128));

    ptr_myclass->do_stuff1();

    MyClass mc(10);

    boost::scoped_array<char> buffer(alloc_buffer());

    buffer[9] = 'd';

    return 0;
}

