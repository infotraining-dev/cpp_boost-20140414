#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>

using namespace std;

struct BigInt
{
    long value;

    BigInt(long value) : value(value) {}

    long abs() const
    {
        return value > 0 ? value : -value;
    }
};


class AbsVisitor : public boost::static_visitor<double>
{
public:
//    double operator()(int x) const
//    {
//        return abs(x);
//    }

//    double operator()(const complex<double>& c) const
//    {
//        return abs(c);
//    }

    double operator()(float fx) const
    {
        return fabs(fx);
    }

    double operator()(const BigInt& x) const
    {
        return x.abs();
    }

    template <typename T>
    double operator()(const T& x) const
    {
        return abs(x);
    }
};

int main()
{
    typedef boost::variant<int, float, complex<double>, BigInt> VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));
    vars.push_back(BigInt(-8888));

    // TODO: korzystając z mechanizmu wizytacji
    // wypisać na ekranie moduły liczb
    cout << "Abs: ";
    AbsVisitor abs_visitor;
    transform(vars.begin(), vars.end(), ostream_iterator<double>(cout, " "),
              boost::apply_visitor(abs_visitor));

    cout << endl;
}
