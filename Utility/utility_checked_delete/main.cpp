#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <algorithm>
#include <boost/checked_delete.hpp>

#include <boost/assign.hpp>

class SomeClass;

SomeClass* create()
{
	return (SomeClass*)0;
}

void simple_test()
{
//    SomeClass* p = create();

//    delete p; // w najlepszym razie warning
}

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
	real_test();

//    std::vector<ToBeDeleted*> vec_ptr
//            = { new ToBeDeleted(), new ToBeDeleted(),
//                new ToBeDeleted(), new ToBeDeleted() };

    using namespace boost::assign;

    std::vector<ToBeDeleted*> vec_ptr;
    vec_ptr += new ToBeDeleted(), new ToBeDeleted, new ToBeDeleted();


    // cleanup
    std::for_each(vec_ptr.begin(), vec_ptr.end(),
                  &boost::checked_delete<ToBeDeleted>);

    std::vector<int> vec_int = list_of(1)(2)(3)(4);
    vec_int += 5, 6, 7, 8, 9, 10;

    for(const auto& x : vec_int)
        std::cout << x << " ";
    std::cout << std::endl;
}
