#include <iostream>
#include <vector>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

template <typename T>
void print_tuple(T& t, string prefix)
{
	cout << boost::tuples::set_open('(')
		 << boost::tuples::set_close(')')
		 << boost::tuples::set_delimiter(',')
		 << prefix << " = " << t << endl;
}

template <typename T1, typename T2>
boost::tuple<T1, T2> my_make_tuple(T1 arg1, T2 arg2)
{
    return boost::tuple<T1, T2>(arg1, arg2);
}

boost::tuple<int, int> find_min_max(const vector<int>& vec)
{
    return boost::tuple<int, int>(*min_element(vec.begin(), vec.end()),
                                  *max_element(vec.begin(), vec.end()));
}

int main()
{

    vector<int> vec_int = { 1, 2, 7, 0, -3, 100, 22, 66 };

    int min, max;
    //boost::tuple<int&, int&> minmax(min, max);
    //minmax = find_min_max(vec_int);
    boost::tie(min, max) = find_min_max(vec_int);

    cout << "min = " << min << "; max = " << max << endl;

    vec_int.push_back(-200);

    boost::tie(min, boost::tuples::ignore) = find_min_max(vec_int);

    cout << "min = " << min << endl;

	// krotka
	boost::tuple<int, double, string> triple(43, 3.1415, "Krotka...");

	// odwolania do krotki
	cout << "triple = ("
		 << triple.get<0>() << ", "
		 << triple.get<1>() << ", "
		 << boost::tuples::get<2>(triple) << ")" << endl;


    // domyslna inicjalizacja krotki
	boost::tuple<short, bool, string> default_tuple;
	print_tuple(default_tuple, "default_tuple");

	// funkcja pomocnicza - make tuple
    short sx = 12;
    triple = boost::make_tuple(sx, 32.222, "Inna krotka...");
	triple = boost::make_tuple(12, 32.222, "Inna krotka...");
	print_tuple(triple, "triple");

	triple.get<2>() = "Inny tekst...";
	print_tuple(triple, "triple");

	// krotki z referencjami
	int x = 10;
	string str = "Tekst...";

	boost::tuple<int&, string&> ref_tpl(x, str);
	ref_tpl.get<0>()++;
	boost::to_upper(ref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    boost::tuple<const int&, string&> cref_tpl
            = boost::make_tuple(boost::cref(x), boost::ref(str));

	//cref_tpl.get<0>()++; // Blad! Referencja do stalej!
	boost::to_lower(cref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;
}
