#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_float.hpp>

using namespace std;

struct MyType
{
    typedef void nested_type;

    int value;
    MyType(int value) : value(value) {}
};

ostream& operator<<(ostream& out, const MyType& mt)
{
    out << mt.value;
    return out;
}

void foo(int x)
{
    cout << "foo(int : " << x << ")\n";
}

//template <typename T>
//void foo(T arg, typename T::nested_type* ptr = 0)
//{
//    cout << "foo(T: " << arg << ")\n";
//}

//template <typename T>
//typename T::nested_type foo(T arg)
//{
//    cout << "foo(T: " << arg << ")\n";
//}

using namespace boost;

template <typename T>
typename disable_if<is_integral<T> >::type foo(T arg)
{
    cout << "foo(T: " << arg << ")\n";
}

template <typename T>
typename enable_if<is_float<T> >::type foo(T arg)
{
    cout << "foo(T: " << arg << ")\n";
}



int main()
{
    foo(6);

    int x = 8;
    foo(x);

    short sx = 9;
    foo(sx);

    foo(3.14);

    MyType mt(101);
    foo(mt);

    return 0;
}

