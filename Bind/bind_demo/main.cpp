#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/assign.hpp>

using namespace std;

void foo(int x, double d, const string& txt)
{
    cout << "foo(x: " << x << ", d:" << d
         << ", txt: " << txt << ")" <<endl;
}

void modify_args(int& x, string& txt)
{
    ++x;

    txt += txt;
}

void print(ostream& out, int x)
{
    out << "x: " << x << endl;
}

class Add //: public std::binary_function<int,int,int>
{
public:
    //typedef int result_type;

    int operator()(int x, int y) const
    {
        return x + y;
    }
};

class Item
{
    bool is_valid_;

    void top_secret()
    {
        cout << "top_secret: ID - " << id_ << endl;
    }

public:
    int id_;

    Item(int id, bool is_valid) : id_(id), is_valid_(is_valid)
    {}

    int id() const
    {
        return id_;
    }

    bool is_valid() const
    {
        return is_valid_;
    }

    void print(string prefix) const
    {
        cout << prefix << " Id: " << id_ << "; IsValid: " << is_valid_ << endl;
    }

    boost::function<void()> get_trojan() const
    {
        return boost::bind(&Item::top_secret, this);
    }
};

int main()
{
    //auto f1 = boost::bind(&foo, _1, 3.14, "Text");

    boost::function<void (int)> f1 = boost::bind(&foo, _1, 3.14, "Text");

    f1(13); // foo(13, 3.14, "Text")

    auto f2 = boost::bind(&foo, 100, _2, _1);

    f2("Tekst", 3.14);

    int x = 10;
    string str = "Text";

    auto m0 = boost::bind(&modify_args, boost::ref(x), boost::ref(str));

    m0();

    cout << "x: " << x << "; str: " << str << endl;

    auto prn = boost::bind(&print, boost::ref(cout), _1);

    prn(14);

    auto a1 = boost::bind<int>(Add(), _1, 10);

    cout << "5 + 10 = " << a1(5) << endl;

    using namespace boost::assign;

    vector<Item*> items;
    items += new Item(1, true), new Item(2, false), new Item(3, true);

//    vector<Item*>::iterator where =
//            find_if(items.begin(), items.end(),
//                    !boost::bind(&Item::is_valid, _1));

    vector<Item*>::iterator where =
            find_if(items.begin(), items.end(),
                    boost::bind(&Item::id_, _1) == 3);

    if (where != items.end())
    {
        (*where)->print("Znalazlem element: " );
    }

    for_each(items.begin(), items.end(),
             boost::bind(&Item::print, _1, "Item - "));

    Item test(6, false);

    boost::function<void()> f = test.get_trojan();

    f();
}

