#include <iostream>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <stdexcept>
#include <boost/shared_ptr.hpp>
#include <memory>

class X
{
public:
    // konstruktor
    X(int value = 0)
        : value_(value)
    {
        std::cout << "Konstruktor X(" << value_ << ")\n";
    }

    // destruktor
    ~X()
    {
        std::cout << "Destruktor ~X(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};


class A
{
	boost::shared_ptr<X> x_;
public:
	A(boost::shared_ptr<X> x) : x_(x) {}
	
	int get_value()
	{
		return x_->value();
	}
};

class B
{
	boost::shared_ptr<X> x_;
public:
	B(boost::shared_ptr<X> x) : x_(x) {}
	
	void set_value(int i)
	{
        x_->set_value(i);
	}	
};

class Base
{
public:
    virtual void foo()
    {
        std::cout << "Base::foo()" << std::endl;
    }

    virtual ~Base() {}
};

class Derived : public Base
{
    X x;
public:
    Derived() : x(13) {}

    virtual void foo()
    {
        std::cout << "Derived::foo()" << std::endl;
    }

    void d_only()
    {
        std::cout << "Derived::d_only()" << std::endl;
    }

    ~Derived() {}
};


int main()
{
	{
		boost::shared_ptr<X> spX1(new X());
		std::cout << "RC: " << spX1.use_count() << std::endl;
		{
			boost::shared_ptr<X> spX2(spX1);
			std::cout << "RC: " << spX2.use_count() << std::endl;
		}
		std::cout << "RC: " << spX1.use_count() << std::endl;
	}

	std::cout << "\n//----------------------\n\n";

    X* raw_ptr = new X(14) ;
    boost::shared_ptr<X> temp(raw_ptr );
    A a(temp);
    try
	{
        B b(temp);
		b.set_value(28);
        throw std::runtime_error("Error");
	}
    catch(const std::runtime_error& e)
    {
        std::cout << e.what() << std::endl;
    }
	
	assert(a.get_value() == 28);
	std::cout << "a.get_value() = " << a.get_value() << std::endl;

    boost::shared_ptr<Base> ptr_a(new Derived());

    ptr_a->foo();

    boost::shared_ptr<Derived> ptr_d
            = boost::dynamic_pointer_cast<Derived>(ptr_a);

    ptr_d->d_only();
}
