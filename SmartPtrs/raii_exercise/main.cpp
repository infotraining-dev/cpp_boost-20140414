#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

class FileGuard : boost::noncopyable
{
public:
    FileGuard(FILE* file) : file_(file)
    {
        if (file_ == 0)
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    ~FileGuard()
    {
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }
private:
    FILE* file_;
};

//class LockGuard : boost::noncopyable
//{
//    Mutex& mtx_;
//public:
//    LockGuard(Mutex& mtx) : mtx_(mtx)
//    {
//        mtx_.lock();
//    }

//    ~LockGuard()
//    {
//        mtx_.unlock();
//    }
//};

//Mutex mtx;

//void thread_safe_foo()
//{
//    LockGuard lk(mtx);

//    //do_stuff();
//}

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file(fopen(file_name, "w"));

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_sp(const char* file_name)
{
    FILE* c_file = fopen(file_name, "w");

    if ( c_file == 0 )
        throw std::runtime_error("Blad otwarcia pliku!!!");

    boost::shared_ptr<FILE> file(c_file, &fclose);

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

struct FileCloser
{
    void operator()(FILE* f)
    {
        fclose(f);
    }
};

void cleanup(void*);

//void foo()
//{
//    boost::shared_ptr<void*>(static_cast<void*>(NULL), cleanup);


//}

void save_to_file_with_up(const char* file_name)
{
    std::unique_ptr<FILE, FileCloser> file(c_file);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    save_to_file_with_sp("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
