#include <iostream>
#include <string>
#include <boost/any.hpp>

#include "PersonalInfo.hpp"

using namespace std;

int main()
{
	cout << "Przykład stosowania klasy any...\n";

	// zmienna typu any
	boost::any a;

    if (a.empty())
        cout << "a jest pusty\n";

	// przypisanie
	a = string("Tekst...");

    a = 1;

    a = 5.1888;

    a = string("Inny tekst...");

	// odczyt
	cout << "a = " << boost::any_cast<string>(a) << endl;

	// kolejne przypisanie - zmiana typu
	a = 42;
	a = 3.1415;

	// odczyt
	cout << "a = " << boost::any_cast<double>(a) << endl;

    double* ptr_a = boost::any_cast<double>(&a);
    cout << "*ptr_a = " << *ptr_a << endl;

	// nieudana konwersja zmiennej typu any - sposób nr 1
	try
	{
		cout << "a = " << boost::any_cast<string>(a) << endl;
	}
	catch(const boost::bad_any_cast& e)
	{
        cout << "Błąd! " << e.what() << endl;
	}

	// nieudana konwersja - sposób 2
	if (string* pstr = boost::any_cast<string>(&a))
		cout << "a = " << (*pstr) << endl;
	else
		cout << "Błąd! Nieudana konwersja z użyciem wskaźnika" << endl;

	// metody any.empty() oraz any.swap()
	boost::any a1(string("Tekst..."));
	boost::any a2 = PersonalInfo("Artur", "Kowalski", 28);
	boost::any a3;

	cout << "a3 ";
	if (!a3.empty())
		cout << "nie";
	cout << "jest pusty" << endl;

    cout << "Przed zamianą:" <<  endl;
	cout << "a1 = " << boost::any_cast<string>(a1) << endl;
	cout << "a2 = " << boost::any_cast<PersonalInfo>(a2) << endl;

	a1.swap(a2);

	cout << "Po zamianie:" <<  endl;
	cout << "a1 = " << boost::any_cast<PersonalInfo>(a1) << endl;
	cout << "a2 = " << boost::any_cast<string>(a2) << endl;

	if (a1.type() == typeid(PersonalInfo))
        cout << "Informacja o typie type_info a1 zgadza się z type_info typu PersonalInfo" << endl;

    int x = 20;

    boost::any temp = x;

    try
    {
        double dx = boost::any_cast<double>(temp);

        double* ptr_x = boost::any_cast<double>(&temp);

        if (ptr_x)
            cout << "temp x = " << *ptr_x << endl;
        else
            cout << "temp x nie jest typu double" << endl;
    }
    catch(const boost::bad_any_cast& e)
    {
        cout << e.what() << endl;
    }
}
