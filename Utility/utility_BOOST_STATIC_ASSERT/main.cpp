#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_pointer.hpp>
#include <boost/shared_ptr.hpp>
#include <memory>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
    BOOST_STATIC_ASSERT(sizeof(int) == 4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
    BOOST_STATIC_ASSERT(boost::is_base_of<Base, T>::value
                        && "T not derived from Base");
    //static_assert(boost::is_base_of<Base, T>::value, "T not derived from Base");
}

template <typename T>
struct is_shared_ptr : public boost::false_type
{};

template <typename T>
struct is_shared_ptr<boost::shared_ptr<T> > : public boost::true_type
{};

//template <typename T>
//struct is_shared_ptr<std::shared_ptr<T> > : public boost::true_type
//{};

template <typename T>
void compatibile_with_raw_pointers_and_shared_ptr(T ptr)
{
    BOOST_STATIC_ASSERT(boost::is_pointer<T>::value ||
                        is_shared_ptr<T>::value);

    std::cout << (*ptr) << std::endl;
}

int main()
{
    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    works_with_base_and_derived(arg);

    int a = 11;
    boost::shared_ptr<int> ptr1(new int(12));
    std::auto_ptr<int> ptr2(new int(13));
    //std::shared_ptr<int> ptr3(new int(14));

    compatibile_with_raw_pointers_and_shared_ptr(&a);
    compatibile_with_raw_pointers_and_shared_ptr(ptr1);
    compatibile_with_raw_pointers_and_shared_ptr(ptr2);
    //compatibile_with_raw_pointers_and_shared_ptr(ptr3);
}
